==== Field Permissions Plus ====

Field Permissions Plus is a replacement for the Content Permissions module that
comes packaged with CCK. Field Permissions Plus expands on the Content 
Permissions module by adding View Own/Edit Own permissions for each field.

Original Developer: Grayside (http://drupal.org/user/346868)
